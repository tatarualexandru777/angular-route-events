import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AgreementComponent } from './agreement/agreement.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import {AppRoutingModule} from "./app-routing.module";
import {RouteService} from "./shared/route.service";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AgreementComponent,
    LandingPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

  ],
  providers: [RouteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
