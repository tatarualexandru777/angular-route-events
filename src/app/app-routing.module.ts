import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./home/home.component";
import {AgreementComponent} from "./agreement/agreement.component";
import {LandingPageComponent} from "./landing-page/landing-page.component";

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'home'
    },
    component: HomeComponent
  },
  {
    path: 'landing-page',
    data: {
      title: 'Landing Page'
    },
    component: LandingPageComponent
  },
  {
    path: 'agreement',
    data: {
      title: 'Agreement Page'
    },
    component: AgreementComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})

export class AppRoutingModule { }
