import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class RouteService {
  private previousRoute: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public previousRoute$: Observable<string> = this.previousRoute.asObservable();

  constructor() {
  }

  setPreviousRoute(previousRoute: string) {
    this.previousRoute.next(previousRoute);
  }
}
