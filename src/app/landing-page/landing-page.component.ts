import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {filter, Subject, takeUntil} from "rxjs";
import {RouteService} from '../shared/route.service';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit, OnDestroy {

  unsubscribe$ = new Subject();
  redirectToAgreements: boolean;

  constructor(private urlService: RouteService, private router: Router) {
    this.router.events
      .pipe(takeUntil(this.unsubscribe$), filter((routerEvent): routerEvent is NavigationEnd => routerEvent instanceof NavigationEnd))
      .subscribe((event) => {
        if (event.id === 1) {
          this.redirectToAgreements = true;
        }
      });
  }

  ngOnInit() {
    this.urlService.previousRoute$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((previousRoute: string) => {
        if (previousRoute === 'agreement') {
          this.redirectToAgreements = false;
        }
      });

    if (this.redirectToAgreements) {
      this.router.navigateByUrl('/agreement');
    }
  }

  ngOnDestroy() {
    // @ts-ignore
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
