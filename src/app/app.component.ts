import {Component, OnDestroy, OnInit} from '@angular/core';
import {NavigationEnd, Router} from "@angular/router";
import {filter, Subject, takeUntil} from "rxjs";
import {RouteService} from "./shared/route.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  previousRoute = '';
  currentRoute = '';
  unsubscribe$ = new Subject();

  constructor(private router: Router, private urlService: RouteService) {
  }

  ngOnInit() {
    this.router.events
      .pipe(takeUntil(this.unsubscribe$), filter((routerEvent): routerEvent is NavigationEnd => routerEvent instanceof NavigationEnd))
      .subscribe((event) => {
        this.previousRoute = this.currentRoute;
        this.currentRoute = event.url;
        this.urlService.setPreviousRoute(this.previousRoute);
      });
  }

  ngOnDestroy() {
    // @ts-ignore
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
